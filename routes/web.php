<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.templatedefault');
});

Auth::routes();

Route::get('/home', 'EventController@index');
Route::get('/events', 'HomeController@index')->name('home');
Route::get('/events_sucess', 'EventController@sucess');
Route::get('/events_cancel/{event_id}', 'EventController@destroy');


Route::resource('events', 'EventController');

Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');