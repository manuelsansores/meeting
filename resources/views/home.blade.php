@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
            <div class="panel panel-info">
                <div class="panel-heading">Agende una cita</div>

                <div class=" panel-info">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="panel-body">
                        {{Form::open(['route' => 'events.store'])}}
                        @if(!empty($error))
                        <div class="alert alert-danger" role="alert">
                            {{$error}}
                        </div>
                        @endif
                        <div class="form-group">
                            <label for="inputstart_Date">Fecha</label>
                            <div class='input-group date'>
                                <input type='text' id="start_date" name="start_date" class="form-control" placeholder="Fecha de reservación" required />
                                <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>

                            </div>
                            <small id="emailHelp" class="form-text text-muted">Introduce una fecha de reservación.</small>
                        </div>
                        <div class="form-group">
                            <div class='input-group date' id='datetimepicker3'>
                                <input type='text' class="form-control" name="end_date" id="end_date" required />
                                <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                                </span>
                            </div>
                            <small id="emailHelp" class="form-text text-muted">Introduce la hora de termino.</small>
                        </div>

                            <button type="submit" class="btn btn-primary">Agendar</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
        $(function () {
            $('#start_date').datetimepicker({
                inline: true,
                sideBySide: true,
                format: 'YYYY-MM-DD H:mm:ss'
            });

            $('#end_date').datetimepicker({
                format: 'H:mm:ss'
            });
        });
    </script>
@endsection