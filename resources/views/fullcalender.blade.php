@extends('layouts.app')

@section('style')
    <link href='{{asset('vendor/fullcalendar/fullcalendar.min.css')}}' rel='stylesheet' />
    <link href='{{asset('vendor/fullcalendar/fullcalendar.print.min.css')}}' rel='stylesheet' media='print' />

@endsection

@section('content')
    <div class="container">
       <div class="row">
           <div class="col-md-12">
               <a href="/events/create" class="btn btn-primary pull-right">Nueva reservación</a>
           </div>
       </div>
    </div>
    <div class="container">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Calendario</div>

                    <div class="panel-body">
                        {!! $calendar->calendar() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="modalCancel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Reservación</h4>
                </div>
                <div class="modal-body">
                    <h4>¿Desea cancelar la reservación?</h4>
                    <input type="hidden" id="event_id">
                    <input type="hidden" id="event_url">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" onclick="cancelEvent()" class="btn btn-danger">Cancelar reservación</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('vendor/fullcalendar/fullcalendar.min.js')}}"></script>
    <script src="{{asset('vendor/fullcalendar/locale/es.js')}}"></script>

    {{--{!! $calendar->calendar() !!}--}}
    {!! $calendar->script() !!}
    <script>
        function openModal(id, url) {
            $('#event_id').val(id);
            $('#event_url').val(url);
            $('#modalCancel').modal('show')
        }
        
        function cancelEvent() {
            var event_id = $('#event_id').val();
            var event_url = $('#event_url').val();
            location.href= event_url+'/'+event_id;
        }
    </script>
@endsection