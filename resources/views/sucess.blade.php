@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Cita</div>

                    <div class="panel-body">
                        <h1> La cita fue agendada con exito, en breve nos comunicaremos con usted </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

