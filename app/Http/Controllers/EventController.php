<?php

namespace App\Http\Controllers;

use App\Event;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;


class EventController extends Controller
{

    public function index()
    {
        $user = Auth::user();

        if(!$user->hasRole('admin')){
            return view('home', compact('calendar'));
        }else{
            $events = [];
            $data = Event::with('user')->get();

            if($data->count()) {
                foreach ($data as $key => $value) {
                    $events[] = Calendar::event(
                        $value->user->name,
                        true,

                        new \DateTime($value->start_date),
                        new \DateTime($value->end_date.' +1 day'),
                        null,
                        // Add color and link on event
                        [
                            'color' => '#f05050',
                            'url' => 'events_cancel',
                            'locale' => 'es',
                            'id' => $value->id
                        ]
                    );
                }
            }
            $calendar = Calendar::addEvents($events)->setCallbacks([ //set fullcalendar callback options (will not be JSON encoded)
                'eventClick' => 'function(event) {
                    openModal(event.id, event.url);
                    return false;
                }']);
            
            return view('fullcalender', compact('calendar'));
        }

    }

    public function create()
    {
        return view('home', compact('calendar'));
    }

    public function store(Request $request)
    {
        $user = Auth::user();
        $name = $user->name;
        $redirect = 'events_sucess';
        if($user->hasRole('admin')){
            $redirect = 'events';
        }

        $start_date = $request->start_date;
        $end_time = date('Y-m-d H:i:s', strtotime(substr($start_date, 0, 10).$request->end_date)) ;
        $total_event = Event::whereBetween('start_date', array($start_date , $end_time))->count();

        if($total_event == 0){
            $event = new Event();
            $event->title = $name;
            $event->start_date = date('Y-m-d H:i:s',strtotime($start_date));
            $event->end_date = $end_time;
            $event->user_id = $user->id;
            $event->save();
            return redirect($redirect);
        }else{
            return view('home', compact('calendar'))->with(array('error' => 'Reservación ocupada, seleccione otra fecha'));
        }
    }

    public function sucess()
    {
        return view('sucess');
    }

    public function destroy($id)
    {
        vent::where('id',$id)->delete();
        return redirect('events');
    }
}
