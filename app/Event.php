<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['title','start_date','end_date'];
    public $allRelations = array();

    public function user()
    {
        return $this->belongsTo(User::class, 'id');
    }
}
